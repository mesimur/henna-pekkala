
package harkka;

// Keeps record of the postoffices info

public class SmartPost {
    
    private String city;
    private String address;
    private String code;
    private String postoffice;
    private String availability;
    private String latitude;
    private String longitude;
    
    public SmartPost(String ci, String ad, String co, String po, String av, String lat, String lon){
        city = ci;
        address = ad;
        code = co;
        postoffice = po;
        availability = av;
        latitude = lat;
        longitude = lon;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getCode() {
        return code;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public String getAvailability() {
        return availability;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    @Override 
    public String toString(){
        return postoffice + ", " + address;
        
    }
    
}
