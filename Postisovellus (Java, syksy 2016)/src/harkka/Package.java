
package harkka;

/**
 * Keeps record of the packageinfo
 */
public abstract class Package {
    private String name;
    private int[] size;
    private double weight;
    private SmartPost startPost;
    private SmartPost destinationPost;
    private int packageClass;
    private double distance;
    
    public Package(String n, int[] s, String w, int pc, SmartPost startP, SmartPost destP, double d){
        name = n;
        size = s;
        weight = Double.parseDouble(w);
        packageClass = pc;
        startPost = startP;
        destinationPost = destP;
        distance = d; 
    }
    
    //Getters:
     public String getPackageName(){
        return name;
    }
    
    public String getPackageSize(){
        String st = Integer.toString(size[0])+"*"+Integer.toString(size[1])+"*"+Integer.toString(size[2]);
        return st;
    }
    
    public double getPackageWeight(){
        return weight;
    }

    public int getPackageClass(){
        return packageClass;
    }

    public SmartPost getStartPost() {
        return startPost;
    }

    public SmartPost getDestinationPost() {
        return destinationPost;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public String toString(){
        return name;
    }
}
