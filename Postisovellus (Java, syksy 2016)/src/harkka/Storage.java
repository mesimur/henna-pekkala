
package harkka;

import java.util.ArrayList;

/**
 * Keeps record of all packages
 * Singleton
 */
public class Storage {
    
    static private Storage st = null;
    private ArrayList<Package> packages = new ArrayList();
    
    public Storage(){ 
    }

    static public Storage getInstance() {
        if (st == null){
            st = new Storage();
        }
        return st;
    }
    
    public void addPackage(Package pg){
        packages.add(pg);
    }
    
    public ArrayList getPackageList(){
        return packages;
    }
    
}
