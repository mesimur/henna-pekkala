
package harkka;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Parses SmartPost data and creates city lists
 * Singleton
 */
public class AllPosts {
    
    static private AllPosts ap = null;
    private Document doc1;
    private ArrayList<City> cityList = new ArrayList<>();

  public AllPosts() {
       try {
            URL url1 = new URL("http://smartpost.ee/fi_apt.xml");

            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(
                new InputStreamReader(url1.openStream()));
            
            String content = "";
            String line;
            
            while((line = br.readLine()) != null) {
                content += line + "\n";
            }
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc1 = dBuilder.parse(new InputSource(new StringReader(content)));
            doc1.getDocumentElement().normalize();
            parseData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            
        }
    }
  
    static public AllPosts getInstance() {
        if (ap == null){
            ap = new AllPosts();
        }
        return ap;
    }
    
    private void parseData() {
        NodeList nodes = doc1.getElementsByTagName("place");
        SmartPost sp;
        City city;
        ArrayList<SmartPost> cityPosts;
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            // Creating a new SmartPost with all informations
            sp = new SmartPost(getValue(e, "city").toUpperCase(), getValue(e, "address"), 
                    getValue(e, "code"), getValue(e, "postoffice"), 
                    getValue(e, "availability"), getValue(e, "lat"), 
                    getValue(e, "lng"));
            
            // Add City name to the list if it is empty
            if (cityList.isEmpty()){
                cityPosts = new ArrayList<>();
                cityPosts.add(sp);
                city = new City(getValue(e, "city").toUpperCase(), cityPosts);
                cityList.add(city);
            } else {
                
                /* Otherwise check if the city already exists in the list
                   If the current cityname exist already in the list, 
                   just add the SmartPost to the city*/
                if (cityList.get(cityList.size()-1).toString().equals(getValue(e, "city").toUpperCase())){
                    cityList.get(cityList.size()-1).getSmartPostList().add(sp);   
                } else {
                    //City doesn't exist yet, let's create it
                    cityPosts = new ArrayList<>();
                    cityPosts.add(sp);
                    city = new City(getValue(e, "city").toUpperCase(), cityPosts);
                    cityList.add(city);
                }
            }
       }
    }

    public ArrayList<City> getcityList() {
        return cityList;
    }
    
     private String getValue(Element e, String tag) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
      
}
    
    
