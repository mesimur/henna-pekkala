/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
/*JEE
*
 *
 * 
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private WebView webView;
    @FXML
    private Button addButton;
    @FXML
    private Button clearButton;
    @FXML
    private ComboBox<City> smartPostCombo;
    @FXML
    private ComboBox<Package> packageCombo;
    @FXML
    private Button makeButton;
    @FXML
    private Button sendButton;
    @FXML
    private TextArea logField;
    @FXML
    private Button infoButton;
    @FXML
    private Button createButton;
    @FXML
    private Button cancelButton;
    @FXML
    private ComboBox<SmartPost> destinationACombo;
    @FXML
    private ComboBox<City> destinationCCombo;
    @FXML
    private RadioButton thirdRadio;
    @FXML
    private RadioButton secondRadio;
    @FXML
    private RadioButton firstRadio;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private TextField weightLabel;
    @FXML
    private TextField sizeLabel;
    @FXML
    private TextField nameLabel;
    @FXML
    private ComboBox<SmartPost> startingACombo;
    @FXML
    private ComboBox<City> startingCCombo;
    @FXML
    private ComboBox<String> readyCombo;
    @FXML
    private Font x1;
    @FXML
    private Label infoLabel;
    @FXML
    private TabPane tabPane;
    @FXML
    private WebView web;
    @FXML
    private ImageView imageView;
    @FXML
    private Font x2;
    @FXML
    private Font x3;
    @FXML
    private Button quitButton1;
    @FXML
    private Button quitButton2;
    @FXML
    private Button clearAllButton;

    //AllPosts parses data, Storage keps trak of made packages. 
    //Also needed file names are written here.
    private AllPosts ap = AllPosts.getInstance();
    private Storage storage = Storage.getInstance();
    private ArrayList<City> cities = ap.getcityList();
    private String fileNameLog = "Loki.txt";
    private String fileNameStorage = "Varasto.txt";
    
    private int flag;

    // Open all images so they can be used
    Image xmas = new Image(getClass().getResourceAsStream("joululahja-paketti.png"));
    Image buffalo = new Image(getClass().getResourceAsStream("buffalo.jpg"));
    Image egg = new Image(getClass().getResourceAsStream("egg.jpg"));
    Image envelope = new Image(getClass().getResourceAsStream("envelope.jpg"));
    Image roses= new Image(getClass().getResourceAsStream("roses.jpg"));
    Image packageImage = new Image(getClass().getResourceAsStream("package-image.jpg"));
    Image troll = new Image(getClass().getResourceAsStream("arm-tickel.jpg"));


    @Override
    public void initialize(URL url, ResourceBundle rb) {
       // Load map
       webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
       // Load invisible map, so can calculate the distance between cities
       web.getEngine().load(getClass().getResource("index.html").toExternalForm());
      
       smartPostCombo.getItems().addAll(cities);
       
       // Create group to the radiobuttons
       ToggleGroup group = new ToggleGroup();
       firstRadio.setToggleGroup(group);
       secondRadio.setToggleGroup(group);
       thirdRadio.setToggleGroup(group);

        // Add ready objects to the creation window
        readyCombo.getItems().add("Paketti");
        readyCombo.getItems().add("Kirje");
  
        // Set color to all components
        addButton.setStyle("-fx-base: #ccccff;");
        clearButton.setStyle("-fx-base: #ccccff;");
        sendButton.setStyle("-fx-base: #ccccff;");
        createButton.setStyle("-fx-base: #ccccff;");
        cancelButton.setStyle("-fx-base: #ccccff;");
        infoButton.setStyle("-fx-base: #ccccff;");
        startingCCombo.setStyle("-fx-base: #ccccff;");
        destinationCCombo.setStyle("-fx-base: #ccccff;");
        startingACombo.setStyle("-fx-base: #ccccff;");
        destinationACombo.setStyle("-fx-base: #ccccff;");
        readyCombo.setStyle("-fx-base: #ccccff;");
        tabPane.setStyle("-fx-base: #ccccff;");
        smartPostCombo.setStyle("-fx-base: #ccccff;");
        firstRadio.setStyle("-fx-base: #ccccff;");
        secondRadio.setStyle("-fx-base: #ccccff;");
        thirdRadio.setStyle("-fx-base: #ccccff;");
        fragileCheck.setStyle("-fx-base: #ccccff;");
        quitButton1.setStyle("-fx-base: #ccccff;");
        quitButton2.setStyle("-fx-base: #ccccff;");

        // Pop-up window
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Question");
        alert.setHeaderText("Halutako ladata valmiina olevat varastot");
        alert.setContentText("Valitse:");

        // Create buttons to the pop-up window
        ButtonType buttonTypeOne = new ButtonType("Kyllä");
        ButtonType buttonTypeTwo = new ButtonType("Ei");
        ButtonType buttonTypeCancel = new ButtonType("Peruuta", ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        
        //User wanted to load storage and log information
        if (result.get() == buttonTypeOne){
            // Open the resent log information
            try{
                BufferedReader in = new BufferedReader(new FileReader(fileNameLog));
                String line = "";
                while ((line = in.readLine()) != null) {
                    logField.setText(logField.getText()+ line + "\n");
                }
                in.close();
            } catch (FileNotFoundException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //Load storage
            try{
            BufferedReader in2 = new BufferedReader(new FileReader(fileNameStorage));
            String line = "";
            String[] info;
            
            //Different packages are seperated with line break
            while ((line = in2.readLine()) != null) {
                //Package info is seperated with "#"
                info=line.split("#");
                int[] array = new int[3];
                String[] size = info[1].split("\\*");
                array[0] = Integer.parseInt(size[0]);
                array[1] = Integer.parseInt(size[1]);
                array[2] = Integer.parseInt(size[2]);
                SmartPost startP =null;
                SmartPost destP = null;
                Package tmp = null;
                
                for (int i = 0; i < cities.size(); i++){
                    //If starting city is found, get the right SmartPost by address info
                    if(cities.get(i).toString().equals(info[4])){
                        for(int j = 0; j < cities.get(i).getSmartPostList().size(); j++){
                            if(cities.get(i).getSmartPostList().get(j).getAddress().equals(info[5])){
                                startP = cities.get(i).getSmartPostList().get(j);
                            }
                        }
                    }
                    
                    //If destination city is found, get the right SmartPost by address info
                    if(cities.get(i).toString().equals(info[6])){
                        for(int k = 0; k < cities.get(i).getSmartPostList().size(); k++){
                            if(cities.get(i).getSmartPostList().get(k).getAddress().equals(info[7])){
                                destP = cities.get(i).getSmartPostList().get(k);
                            }
                        }
                    }
                }
                //Depending on the package class, create different types of packages
                if (Integer.parseInt(info[3]) == 1){
                    tmp= new FirstClass(info[0], array, info[2], 1,  startP, destP, Double.parseDouble(info[8]));
                }
                else if (Integer.parseInt(info[3]) == 2){
                    tmp= new SecondClass(info[0], array, info[2], 2,  startP, destP, Double.parseDouble(info[8]), Boolean.parseBoolean(info[9]));
                }
                else if (Integer.parseInt(info[3]) == 3){
                    tmp= new ThirdClass(info[0], array, info[2], 3,  startP, destP, Double.parseDouble(info[8]));
                }
                storage.addPackage(tmp);
                packageCombo.getItems().add(tmp);
            }
             in2.close();
             flag = 1;

            } catch (FileNotFoundException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
           } catch (IOException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        //User didn't want to load storage and log
        else if (result.get() == buttonTypeTwo) {
            flag = 2;
            //We don't have to do anything :-) 
        } 
        
        //User  pressed cancel 
        else {
            //Quit program
            System.exit(0);
        }

        // Daystamp for the log
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        logField.setText(logField.getText() + "Istunto "+ dtf.format(now) + "\n"
                        + "Varastossa on " +storage.getPackageList().size() + " pakettia.\n");
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }    

    @FXML
    private void addMap(ActionEvent event) {
        // Add the selected city to the creation window
        if (smartPostCombo.getValue() != null){
            City city = smartPostCombo.getValue();
            if (!startingCCombo.getItems().contains(city)){       
                startingCCombo.getItems().add(city);
                destinationCCombo.getItems().add(city);
            }
            SmartPost tmp;
            
            // Add all postoffices of the city to the map
            for (int i = 0; i < ap.getcityList().size(); i++){
                if(city.toString().equals(ap.getcityList().get(i).toString())){
                    for (int j = 0; j < ap.getcityList().get(i).getSmartPostList().size(); j++){
                        tmp = ap.getcityList().get(i).getSmartPostList().get(j);
                        webView.getEngine().executeScript("document.goToLocation('" +
                            tmp.getAddress() + ", " + tmp.getCode() + " " + 
                            tmp.getCity() + "', '" + tmp.getPostoffice() + ".<br /> Aukioloajat:  " 
                              + tmp.getAvailability() + "', 'red')");      
                    }
                }
             }
        }
        smartPostCombo.getSelectionModel().clearSelection();
    }

    // Clear alla phats and offices from the map
    @FXML
    private void clearAllMap(ActionEvent event) {
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }

    //Clear paths leave marks
    @FXML
    private void clearMap(ActionEvent event) {
        webView.getEngine().executeScript("document.deletePaths()");
        
    }

    //Take user to creation tab
    @FXML
    private void createPackage(ActionEvent event) throws IOException {
        // Change the tab
        tabPane.getSelectionModel().selectNext();
    }

    //Draw the path
    @FXML
    private void sendPackage(ActionEvent event) {
        if (packageCombo.getValue() != null){
            Package toBeSent = packageCombo.getValue();
            
            // List the cordinates with the offices between the package
            ArrayList<String> coordinates = new ArrayList<>();
            coordinates.add(toBeSent.getStartPost().getLatitude());
            coordinates.add(toBeSent.getStartPost().getLongitude());
            coordinates.add(toBeSent.getDestinationPost().getLatitude());
            coordinates.add(toBeSent.getDestinationPost().getLongitude());
            
            // Draw the path with right colour between the offices ;)
            if (toBeSent.getPackageName().equals("Joululahja")){
                webView.getEngine().executeScript("document.createPath(" + coordinates
                        + ", 'red', " + toBeSent.getPackageClass() +")");
            }
            else if(toBeSent.getPackageName().equals("Kukkia")){
                webView.getEngine().executeScript("document.createPath(" + coordinates
                        + ", 'green', " + toBeSent.getPackageClass() +")");
            }
            else{
                webView.getEngine().executeScript("document.createPath(" + coordinates
                        + ", 'blue', " + toBeSent.getPackageClass() +")");
            }
            
            // Write all information of the sended package to the log
            storage.getPackageList().remove(toBeSent);
            logField.setText(logField.getText() + "Paketti " +toBeSent.toString() +  " lähetetty. Paketin tiedot:"
                            + "\nNimi: " + toBeSent.toString()
                            + "\nLuokka: " + toBeSent.getPackageClass()
                            + "\nKoko: " + toBeSent.getPackageSize()
                            + "\nPaino: " + toBeSent.getPackageWeight()
                            + "\nLähetyspaikka: " + toBeSent.getStartPost().toString() 
                            + ", " + toBeSent.getStartPost().getCity()
                            + "\nVastaanottopaikka: " + toBeSent.getDestinationPost().toString()
                            + ", " + toBeSent.getDestinationPost().getCity()
                            + "\nKuljettumatka: " + toBeSent.getDistance()
                            + "\nVarastossa on nyt "+ storage.getPackageList().size() + " pakettia.\n");
            
            // Removes sended package from the storage
            storage.getPackageList().remove(toBeSent);
            //Refresh the package combobox
            packageCombo.getItems().clear();
            packageCombo.getItems().addAll(storage.getPackageList());
        
        }
       
    }

    // Shows the information window of the packageclasses
    @FXML
    private void showInfo(ActionEvent event) throws IOException {
        Stage packageCreation = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLinfo.fxml"));
        Scene scene = new Scene(page);
        packageCreation.setScene(scene);
        packageCreation.show();
    }

    //Creates the made package
    @FXML
    private void createCreation(ActionEvent event) {
        
        ArrayList<String> geoPoints = new ArrayList();
        int[] size = new int[3];
        SmartPost startPost = null;
        SmartPost destinationPost = null;
        City startCity = null;
        City destinationCity = null;

        // Check if all wanted information is filled
        if (nameLabel.getText().isEmpty()){
            // ERROR, no name
            infoLabel.setText("Anna paketille nimi");
            
        } else if (sizeLabel.getText().isEmpty()){
            // ERROR, no weight
            infoLabel.setText("Anna paketille koko");
            
        } else if (weightLabel.getText().isEmpty()){
            // ERROR, no size
            infoLabel.setText("Anna paketille paino");
            
        } else if (startingCCombo.getValue() == null){
            // ERROR, not start city picked
            infoLabel.setText("Anna paketille lähtökaupunki");
            
        } else if(destinationCCombo.getValue() == null){
            // ERROR, not destination city picked
            infoLabel.setText("Anna paketille kohdekaupunki");
        
        } else if (startingACombo.getValue() == null){
            // ERROR, not destination automat picked
            infoLabel.setText("Anna paketille lähtöautomaatti");
            
        } else if (destinationACombo.getValue() == null){
            // ERROR, not start automat picked
            infoLabel.setText("Anna paketille kohdeautomaatti");
            
        } else {
            // All good this far
            // Now try if all information is correct type and size
            String[] temp = sizeLabel.getText().split("\\*");
            try {
                size[0] = Integer.parseInt(temp[0]);
                size[1] = Integer.parseInt(temp[1]);
                size[2] = Integer.parseInt(temp[2]);
                double weight = Double.parseDouble(weightLabel.getText());
                
                // If the packagesize and weight is in the limits, continue checking
                if (size[0] > 0 && size[1] > 0 && size[2] > 0 && weight > 0 && 
                    size[0] <= 100 && size[1] <= 100 && size[2] <= 100 && weight <= 70){
                    
                    // Get the right cities from Citylist
                    for (City i : cities){
                        // Get the right cities from Citylist
                        if (i.toString().equals(startingCCombo.getValue().toString())){
                            startCity = i;
                        }
                        if (i.getName().equals(destinationCCombo.getValue().toString())){
                            destinationCity = i;   
                        }
                    }
                    
                    // Get the right postoffices
                    for(SmartPost j : startCity.getSmartPostList()){
                        if(j.toString().equals(startingACombo.getValue().toString())){
                                startPost = j;
                        }
                    }

                    for(SmartPost j : destinationCity.getSmartPostList()){
                        if(j.toString().equals(destinationACombo.getValue().toString())){
                            destinationPost = j; 
                        }
                    }

                    if (startPost.toString().equals(destinationPost.toString()) && destinationCity.toString().equals(startCity.toString())){
                        // ERROR, trying to send to the same destination
                        infoLabel.setText("Et voi lähettää pakettia samaan automaattiin!");
                    
                    } else {
                        geoPoints.add(startPost.getLatitude());
                        geoPoints.add(startPost.getLongitude());
                        geoPoints.add(destinationPost.getLatitude());
                        geoPoints.add(destinationPost.getLongitude());
                        
                        // Calculate distance between postoffices
                        double distance = (double) (web.getEngine()).executeScript("document.createPath(" + geoPoints + ", 'red', '1')");

                        // Check if the package fits to the selected class
                        //User has picked first class
                        if (firstRadio.isSelected()){
                            
                            if (distance > 150 ){
                                // ERROR, trying to send too far
                                infoLabel.setText("Matka liian pitkä!");
                            }
                            else if (fragileCheck.isSelected()){
                                // ERROR, package cannot be fragile in thiss class
                                infoLabel.setText("Paketti ei voi olla särkyvää tässä luokassa!");
                            
                            } else {
                                // All good everything, lets make some packages
                                Package pg = new FirstClass(nameLabel.getText(), size, weightLabel.getText(), 1, startPost, destinationPost, distance);
                                storage.addPackage(pg);
                                
                                // Write packageinfo to the log
                                String s = "Paketti " + pg.toString() + " luotiin.\nVarastossa on nyt " 
                                        +storage.getPackageList().size() + " pakettia.\n";
                                logField.setText(logField.getText()+ s);
                                packageCombo.getItems().add(pg);
                                closeCreation();
                            }
                        
                        } else if (secondRadio.isSelected()) {
                            //Second class is picked
                            if (size[0] > 40 || size[1] > 40 || size[2] > 40) {
                                // ERROR, too big 
                                infoLabel.setText("Liian suuri paketti tähän luokkaan!");
                            
                            } else {
                                // All good everything, lets make some packages
                                Package pg = new SecondClass(nameLabel.getText(), size, weightLabel.getText(), 2, startPost, destinationPost, distance, fragileCheck.isSelected());
                                storage.addPackage(pg);
                                String s = "Paketti " + pg.toString() + " luotiin.\nVarastossa on nyt " 
                                        +storage.getPackageList().size() + " pakettia.\n";
                                logField.setText(logField.getText()+ s);
                                packageCombo.getItems().add(pg);
                                closeCreation();
                            }
                        
                        } else if (thirdRadio.isSelected()){
                            //Third class is picked
                            if (fragileCheck.isSelected()){
                                // ERROR, cannot be fragile in this class
                                infoLabel.setText("Paketti ei voi olla särkyvää tässä luokassa!");
                            
                            } else {
                                // All good everything, lets make some packages
                                Package pg = new ThirdClass(nameLabel.getText(), size, weightLabel.getText(), 3, startPost, destinationPost, distance);
                                storage.addPackage(pg);
                                String s = "Paketti " + pg.toString() + " luotiin.\nVarastossa on nyt " 
                                        +storage.getPackageList().size() + " pakettia.\n";
                                logField.setText(logField.getText() + s);
                                packageCombo.getItems().add(pg);
                                closeCreation();
                            }
                        } else if (thirdRadio.isSelected() == false && firstRadio.isSelected() == false
                                   && secondRadio.isSelected() == false) {
                            // ERROR, no class picked
                            infoLabel.setText("Valitse luokka!");

                        }
                    }
                } else {
                    //Weight or size aren't possible
                    infoLabel.setText("Paketilla täytyy olla järkevä paino ja koko!\nKoko voi olla maksimissaan 100*100*100 cm ja paino 70 kg.");
                }
            } catch(NumberFormatException nfe) {
                infoLabel.setText("Anna koko ja paino oikeassa muodossa!");
            } catch (ArrayIndexOutOfBoundsException aio) {
                infoLabel.setText("Anna paketin koko cm*cm*cm!");
            }
        }    
    }

    @FXML
    private void cancelCreation(ActionEvent event) {
        // Close the creationtab
        closeCreation();
    }

    // Add all offices from the celected city to the destination office comboBox
    @FXML
    private void readDestinationCCombo(ActionEvent event) {
        destinationACombo.getItems().clear();
        //Get the selected city
        if(startingCCombo.getValue() != null){
            City city = destinationCCombo.getValue();
            ArrayList<SmartPost> sPosts = null;
            
            for(City i : ap.getcityList()){
                if(city.toString().equals(i.toString())){
                    sPosts = i.getSmartPostList();
                }
            }
            //Adds the offices
            if(!sPosts.isEmpty()){
                destinationACombo.getItems().addAll(sPosts);
            }
        }
    }

    // Add all offices from the celected city to the starting office comboBox
    @FXML
    private void readStartingCCombo(ActionEvent event) {
        startingACombo.getItems().clear();
        //Get the selected city
        if(startingCCombo.getValue() != null){
            City cityName = startingCCombo.getValue();
            ArrayList<SmartPost> sPosts = null;

            for(City i : ap.getcityList()){
                if(cityName.toString().equals(i.toString())){
                    sPosts = i.getSmartPostList();
                }
            }
            //Adds the offices
            if(!sPosts.isEmpty()){
                startingACombo.getItems().addAll(sPosts);
            }
        }
    }

    
    /* Sets all the creation information to null
    and goes back to the first tab*/
    private void closeCreation(){
        readyCombo.valueProperty().set(null);
        nameLabel.clear();
        sizeLabel.clear();
        weightLabel.clear();
        fragileCheck.setSelected(false);
        firstRadio.setSelected(false);
        secondRadio.setSelected(false);
        thirdRadio.setSelected(false);
        startingCCombo.getSelectionModel().clearSelection();
        startingACombo.getSelectionModel().clearSelection();
        destinationCCombo.getSelectionModel().clearSelection();
        destinationACombo.getSelectionModel().clearSelection();
        infoLabel.setText("");
        tabPane.getSelectionModel().selectFirst();    
    }
   
    
    // Some readymade packages
    @FXML
    private void setReadyPackage(ActionEvent event) {
        // Set the packageinfo to the labels when picked
        if(readyCombo.getValue()!= null){
            if (readyCombo.getValue().equals("Paketti")){
                nameLabel.setText("Paketti");
                sizeLabel.setText("20*30*25");
                weightLabel.setText("3");
            } else if (readyCombo.getValue().equals("Kirje")){
                nameLabel.setText("Kirje");
                sizeLabel.setText("1*20*10");
                weightLabel.setText("0.4");
            } 
        }
    } 

    // Write the log, save the storage items and close window
    @FXML
    private void quitProgram(ActionEvent event) throws IOException {
        //If user has loaded log and storage
        
        if (flag == 1){
        //Writes the log
        String textLog = logField.getText() + "\n";
        BufferedWriter out1 = new BufferedWriter(new FileWriter(fileNameLog));
        out1.write(textLog);
        out1.close();
        
        //Saves storage items
        String textStorage = "";
        BufferedWriter out2 = new BufferedWriter(new FileWriter(fileNameStorage));
        for(int i = 0; i < storage.getPackageList().size(); i++){
            //If the package's class is 2, save the fragile check too
            if(((Package)storage.getPackageList().get(i)).getPackageClass()==2){
                SecondClass tmp =(SecondClass) storage.getPackageList().get(i);
                textStorage = textStorage + tmp.getPackageName()+"#"+tmp.getPackageSize()
                    +"#"+Double.toString(tmp.getPackageWeight())+"#"+tmp.getPackageClass()+"#"+tmp.getStartPost().getCity()
                    +"#"+tmp.getStartPost().getAddress()+"#"+tmp.getDestinationPost().getCity()
                    +"#"+tmp.getDestinationPost().getAddress()+"#"+tmp.getDistance()+"#"+tmp.getFragile()+"\n";
            }
            else{
                Package tmp =(Package) storage.getPackageList().get(i);
                textStorage = textStorage + tmp.getPackageName()+"#"+tmp.getPackageSize()
                    +"#"+Double.toString(tmp.getPackageWeight())+"#"+tmp.getPackageClass()+"#"+tmp.getStartPost().getCity()
                    +"#"+tmp.getStartPost().getAddress()+"#"+tmp.getDestinationPost().getCity()
                    +"#"+tmp.getDestinationPost().getAddress()+"#"+tmp.getDistance()+"\n";
            }
        }
        out2.write(textStorage);
        out2.close();
        
        //User didn't want to load storage. 
        //We shall add the log and packages that were created to existing files
        } else {
            //Appends the log to the existing one
            String textLog = logField.getText() + "\n";
            BufferedWriter out1 = new BufferedWriter(new FileWriter(fileNameLog, true));
            out1.write(textLog);
            out1.close();
            
            //Appends created packages to the storage
            String textStorage = "";
            BufferedWriter out2 = new BufferedWriter(new FileWriter(fileNameStorage, true));
            for(int i = 0; i < storage.getPackageList().size(); i++){
                //If the package's class is 2, save the fragile check too
                if(((Package)storage.getPackageList().get(i)).getPackageClass()==2){
                    SecondClass tmp =(SecondClass) storage.getPackageList().get(i);
                    textStorage = textStorage + tmp.getPackageName()+"#"+tmp.getPackageSize()
                        +"#"+tmp.getPackageWeight()+"#"+tmp.getPackageClass()+"#"+tmp.getStartPost().getCity()
                        +"#"+tmp.getStartPost().getAddress()+"#"+tmp.getDestinationPost().getCity()
                        +"#"+tmp.getDestinationPost().getAddress()+"#"+tmp.getDistance()+"#"+tmp.getFragile()+"\n";
                }
                else{
                    Package tmp =(Package) storage.getPackageList().get(i);
                    textStorage = textStorage + tmp.getPackageName()+"#"+tmp.getPackageSize()
                        +"#"+tmp.getPackageWeight()+"#"+tmp.getPackageClass()+"#"+tmp.getStartPost().getCity()
                        +"#"+tmp.getStartPost().getAddress()+"#"+tmp.getDestinationPost().getCity()
                        +"#"+tmp.getDestinationPost().getAddress()+"#"+tmp.getDistance()+"\n";
                }
            }
            out2.write(textStorage);
            out2.close();
        }
        
        //Close windows
        closeWindow(quitButton1);
        closeWindow(quitButton2);
    }
    
    
    // Put images when created package is selected
    @FXML
    private void selectPackage(ActionEvent event) {
        if (packageCombo.getValue() != null)  {
            if (packageCombo.getValue().toString().equals("Joululahja")){
                imageView.setImage(xmas);
            } else if (packageCombo.getValue().toString().equals("Kukkia")) {
                imageView.setImage(roses);
            } else if (packageCombo.getValue().toString().equals("Puhveli")){
                imageView.setImage(buffalo);
            } else if (packageCombo.getValue().toString().equals("Suklaamuna")){
                imageView.setImage(egg);
            } else if (packageCombo.getValue().toString().equals("Kirje")) {
                imageView.setImage(envelope);
            } else if (packageCombo.getValue().toString().equals("Kainalon kutittaja")) {
                imageView.setImage(troll);
            } else {
                imageView.setImage(packageImage);
            }
        } else {
            imageView.setImage(null);
        }
    }

   //Bye bye!
    private void closeWindow(Button button){
        Stage scene = (Stage) button.getScene().getWindow();
        scene.close();
    }
}
