/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harkka;

/**
 *
 * 
 */
public class SecondClass extends Package{

    private boolean fragile;
    
    public SecondClass(String n, int[] s, String w, int pc, SmartPost startP, SmartPost destP, double dist, boolean f) {
        super(n, s, w, pc, startP, destP, dist);
        //Second class is the only one with fragile check
        fragile = f; 
    }
    
    public boolean getFragile(){
        return fragile;
    }
}
