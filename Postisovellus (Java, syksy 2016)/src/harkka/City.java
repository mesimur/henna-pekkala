
package harkka;

import java.util.ArrayList;

// Keeps record of the all offices in city
 
public class City {
    private String name;
    private ArrayList<SmartPost> smartPostList = new ArrayList<>();
    
    public City(String n, ArrayList spl){
       name = n;
       smartPostList = spl;
        
    }

    public String getName() {
        return name;
    }

    public ArrayList<SmartPost> getSmartPostList() {
        return smartPostList;
    }
    
    @Override
    public String toString() { 
        return name;
    }
    
}
