/*
Resources:

Javascript line breaks:
http://stackoverflow.com/questions/16886366/javascript-line-breaks

Radio button group:
http://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/RadioButton.html

Popup Window:
http://code.makery.ch/blog/javafx-dialogs-official/

How to use imageView:
http://stackoverflow.com/questions/22710053/how-can-i-show-an-image-using-the-imageview-component-in-javafx-and-fxml
 */
package harkka;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
 * Made by: Karoliina Varso (0456097) and  Henna Pekkala (0456204)
 */
public class Harkka extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        
        // Add orange background
        root.setStyle("-fx-background-image: url('" + getClass().getResource("orange-background.jpg").toExternalForm() + "')");   
    }

    public static void main(String[] args) {
        launch(args);
    }
}
