# README #

### What is this repository for? ###

Henna Pekkalan Lappeenrannan teknillisessä yliopistossa tekemät harjoitustyöt.

Javalla vuoden 2016 syksyllä tehty graafinen, JavaScript rajapintaa hyödyntävä, postisovellus tehtiin parityönä. Ohjelmalla voi etsiä ja piirtää kartalle postiautomaatteja sekä luoda ja lähettää paketteja. Automaatti pitää olla piirrettynä kartalle, jotta siitä/sinne voi lähettää paketin. Kansiossa on myös luokkakaavio ja raportti, josta ilmenee tehtävän jako ja ohjelman toiminnot.

C:llä kolmen hengen ryhmässä tehty harjoitustyö vuoden 2016 keväällä oli pullonpalautusautomaatti. Ohjelmassa voi syöttää pulloja koneeseen ja palautuksen lopuksi se tulostaa kuitin. Ohjelma pitää myös päälokia, johon kirjataan jokaisen palautussession aika, palautettujen pullojen määrä ja panttien summa. Itse olin vastuussa tiedostojen käsittelystä, eli pää- ja tilapäislokin avaamisesta, päivittämisestä ja sulkemisesta sekä tuotetietojen lukemisesta.

Pythonilla syksyllä 2015 tein säätietojen käsittelyohjelman. Ohjelma lukee Ilmatieteenlaitoksen .csv-päätteisiä kaupunkien 30-päivän säätietoja ja poimii sieltä tietoja käyttäjälle. Ohjelma voi piirtää myös graafin yhden kaupungin säästä tai kahden kaupungin vertailukuvan. Tiedostojen nimet täytyy kirjoittaa ohjelmaan päätteineen.