/* Include guardit estämään saman tiedoston #includeeminen useampaan kertaan
    https://en.wikipedia.org/wiki/Include_guard */
#ifndef TIEDOSTO_H_INCLUDED
#define TIEDOSTO_H_INCLUDED

#include <stdio.h>

void lue_tuotetiedot_listaan(Tuote**);

FILE* avaa_tilapaisloki(void);
FILE* avaa_paaloki(void);

void kirjoita_tilapaisloki(FILE*, char*, float, float);
void kirjoita_paaloki(FILE*, int, float);

void sulje_loki(FILE**);

#endif
