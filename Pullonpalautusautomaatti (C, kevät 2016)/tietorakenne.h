/* Include guardit estämään saman tiedoston #includeeminen useampaan kertaan
    https://en.wikipedia.org/wiki/Include_guard */
#ifndef TIETORAKENNE_H_INCLUDED
#define TIETORAKENNE_H_INCLUDED

/* Määritellään tuotelistan alkio*/
struct s_tuote {
    char* tyyppi; /* Pullon tyyppi */
    float koko;   /* Pullon koko */
    float pantti; /* Pullon pantti */
    struct s_tuote *pSeuraava;
};
typedef struct s_tuote Tuote;

/* Määritellään palautuslistan alkio */
struct s_palautus {
    int index; /* Pullon indeksi, vastaa sijaintia tuotelistassa */
    struct s_palautus *pSeuraava;
};
typedef struct s_palautus Palautus;


void lisaa_tuote(Tuote**, char*, float, float);
void lisaa_palautus(Palautus**, int);

Tuote *etsi_tuote(Tuote*, int);

int tuote_pituus(Tuote*);
int palautus_pituus(Palautus*);

void tyhjenna_tuotelista(Tuote**);
void tyhjenna_palautuslista(Palautus**);


#endif
