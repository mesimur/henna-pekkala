/* Include guardit estämään saman tiedoston #includeeminen useampaan kertaan
    https://en.wikipedia.org/wiki/Include_guard */
#ifndef FUNKTIO_H_INCLUDED
#define FUNKTIO_H_INCLUDED

#include "tietorakenne.h"

#define TIMESTAMP_LENGTH 17 /* strlen("dd.mm.yyyy hh:mm") + 1 */

/* Aikaleimat */
void timestamp(char*);

/* Valikot */
int paa_valikko(void);
int palautus_valikko(Tuote*);

float kokonaispantti(Tuote*, Palautus*);

/* Kuitti */
void kuitti(Tuote*, Palautus*);

#endif
