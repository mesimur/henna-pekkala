/*
 * CT60A0210 Käytännön ohjelmointi -kurssin ohjelmien otsikkotiedot.
 * Tekijä: Jani Purhonen
 * Opiskelijanumero: 0456848
 * Päivämäärä: 29.4.2016
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 * Ryhmä:
 * 	Jani Purhonen
 * 	Henna Pekkala
 * 	Karoliina Varso
 *
 * Lähteet:
 *     - Kurssin C-opas
 */

#include <stdio.h>

#include "funktio.h"
#include "tiedosto.h"
#include "tietorakenne.h"

int main(void) {
    /* Listat */
    Tuote*    tuote_lista    = NULL;
    Palautus* palautus_lista = NULL;

    /* Avataan lokitiedostot */
    FILE* temp_log = avaa_tilapaisloki();
    FILE* perm_log = avaa_paaloki();

    /* Valikon muuttujat */
    int valinta;         /* Käyttäjän valinta valikossa */
    int should_quit = 0; /* Merkkaa pitäisikö ohjelmasta poistua */
    int valikko_tila  = 0; /* Valikon tila, 0 = Päävalikko, 1 = palautusvalikko */
    Tuote* palautustuote = NULL; /* Käyttäjän palauttama tuote */

    /* Lue tuotetiedot listaan */
    lue_tuotetiedot_listaan(&tuote_lista);

    printf("\nPULLONPALAUTUSAUTOMAATTI\n\n");

    /* Päälooppi */
    while (!should_quit) {
        /* Tulostetaan oikea valikko. Päävalikosta on mahdollista joko siirtyä
           palautusvalikkoon tai poistua ohjelmasta. */
        if (valikko_tila == 0) { /* Päävalikko */
            valinta = paa_valikko();

            switch (valinta) {
                case 1:
                    valikko_tila = 1; /* Siirry palautusvalikkoon */
                    break;
                case 0:
                    should_quit = 1; /* Aloitetaan ohjelmasta poistuminen */
                    printf("\nSuljetaan pullonpalautusautomaatti.\n");
                    break;
                default:
                    printf("\nVäärä valinta!\n\n");
            }

        } else if (valikko_tila == 1) { /* Palautusvalikko */
            valinta = palautus_valikko(tuote_lista); /* Palautusvalikkoa varten tarvitaan lista tuotteista */

            /* Validien tuotteiden koodit ovat > 0, Kuitin tulostamiselle on varattu koodi == 0
               Negatiiviset luvut ovat varattuja erilaisille virhekoodeille ja
               ne käsitellään erikseen */
            if (valinta > 0) {
                palautustuote = etsi_tuote(tuote_lista, valinta); /* Valitaan tuotelistasta valittu tuote */

                if (palautustuote != NULL) {
                    printf("Syötetty: %s %.2fl\n", palautustuote->tyyppi, palautustuote->koko);
                    lisaa_palautus(&palautus_lista, valinta); /* Lisätään tuote palautuslistaan */
                    kirjoita_tilapaisloki(temp_log, palautustuote->tyyppi, palautustuote->koko, palautustuote->pantti);
                } else {
                    printf("Virheellinen pullo\n");
                }
            } else if (valinta == 0) {
                /* Tulosta kuitti, kirjoita pääloki ja nollaa palautukset */
                kuitti(tuote_lista, palautus_lista);
                kirjoita_paaloki(perm_log, palautus_pituus(palautus_lista), kokonaispantti(tuote_lista, palautus_lista));
                tyhjenna_palautuslista(&palautus_lista);

                printf("\n\nPULLONPALAUTUSAUTOMAATTI\n\n\n"); /* Päävalikkoon palatessa täytyy tulostaa viesti uudelleen */
                valikko_tila = 0; /* Palataan päävalikkoon */
            } else { /* valinta < 0 */
                /* Käsittele virheet */
                printf("Väärä valinta!\n");
            }
        }
    }

    /* Lopetaan ohjelma vapauttamalla varatut muistit ja sulkemalla avoimet tiedostot */

    /* Vapauta varattu muisti */
    tyhjenna_tuotelista(&tuote_lista);
    tyhjenna_palautuslista(&palautus_lista);

    /* Sulke lokitiedostot */
    sulje_loki(&temp_log);
    sulje_loki(&perm_log);

    return 0;
}
