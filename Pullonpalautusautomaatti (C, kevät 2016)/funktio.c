/*
 * CT60A0210 Käytännön ohjelmointi -kurssin ohjelmien otsikkotiedot.
 * Tekijä: Jani Purhonen
 * Opiskelijanumero: 0456848
 * Päivämäärä: 29.4.2016
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 * Ryhmä:
 * 	Jani Purhonen
 * 	Henna Pekkala
 * 	Karoliina Varso
 *
 * Lähteet:
 *     - Kurssin C-opas
 *     - stdin tyhjentäminen: http://stackoverflow.com/a/12730213
 *     - Kellonaikojen formatointi: http://linux.die.net/man/3/strftime
 */

#include "funktio.h"

#include <time.h>
#include <stdio.h>
#include <string.h>

#define VIOPE 0 /* Merkkaa kompiloidaanko viopespesiffi koodi, vaihda myös tiedosto.c */

/**
 * Tallentaa tämänhetkisen timestampin muodossa dd.mm.yyyy hh:mm annettavaan stringiin
 * Oikean timestampin koon määrittelee TIMESTAMP_LENGTH
 *
 * Esimerkki:
 * 		char testi[TIMESTAMP_LENGTH];
 * 		timestamp(testi);
 *   	printf("%s", testi); --> 14.04.2016 16:53
 */
void timestamp(char* str) {
    struct tm *now = NULL;
    time_t timer = time(NULL);

    /* Muunna time_t -> struct tm */
    now = localtime(&timer);

    /* Tallenna aika */
    strftime(str, TIMESTAMP_LENGTH, "%d.%m.%Y %H:%M", now);
}


/**
 * Tulostaa päävalikon ja palauttaa käyttäjän valinnan
 */
int paa_valikko(void) {
    int valinta;  /* Käyttäjän valinta */
    char rivi[3]; /* stdin luettu rivi, varattu tila riittää yhdelle numerolle, \n ja \0 merkeille */

    printf("Automaatti on valmis ottamaan vastaan pullot ja tölkit.\n");
    printf("1) Aloita palautus\n");
    printf("0) Lopeta\n\n");
    printf("Valitse: ");

    /* Lue ja tarkista alustavasti tarkista luettu rivi */
    if (fgets(rivi, sizeof(rivi), stdin) == NULL) {
        return -1;
    }

    /**
     * http://stackoverflow.com/a/12730213
     *
     * Inputin lukemisessa oli bugi, jos käyttäjä antaa yli 1 merkkiä pitkän
     * inputin, stdin jäi loppuinputti "jonoon", kun stdin seuraavan kerran luetaan
     * (todennäköisesti tässä samassa funktiossa), käyttäjän inputin sijasta luetaankin
     * jonossa oleva stdin, mikä johtaa looppimaiseen käytökseen.
     *
     * Korjattiin lisäämällä looppi joka lukee stdin tyhjäksi jos inputti ei pääty
     * newlineen (eli inputti ei mahdu rivibufferiin) StackOverflow vastauksen mukaisesti
     */
     if (rivi[strlen(rivi) - 1] != '\n') {
         int c;
         while ((c = fgetc(stdin)) != EOF && c != '\n');
     }

     /* Erottele inputista käyttäjän valinta */
     if (sscanf(rivi, "%d", &valinta) != 1) {
         return -1;
     }

     /* Tarkista vielä että valinta on sallituissa rajoissa */
     if (valinta < 0 || valinta > 1) {
         return -1;
     }

     return valinta;
}


/**
 * Tulostaa palautusvalikon perustuen tuotelistaan ja palauttaa käyttäjän
 * valinnan
 *
 * lista -> Tuotelista jonka mukaan valikko tulostetaan
 */
int palautus_valikko(Tuote* lista) {
    if (lista == NULL) {
        /* Lista oli tyhjä */
        return -3;
    }

    char rivi[21]; /* strlen("18446744073709551615") + 1 */
    Tuote* nykyinen = lista;
    int indeksi = 1;
    int valinta;

    printf("\nSyötä uusi pullo tai tölkki.\n");

    /* Tulosta tuotteet */
    while (nykyinen != NULL) {
        printf("%d) %s %.2fl\n", indeksi, nykyinen->tyyppi, nykyinen->koko);

        /* Etene listassa */
        nykyinen = nykyinen->pSeuraava;
        indeksi++;
    }

    /* Indeksi merkkaa nyt numeroa jolla pitäisi tulostaa kuitti */
    printf("%d) Lopeta syöttö ja tulosta kuitti.\n", indeksi);
    printf("--> ");

    /* Lue syöte */
    if (fgets(rivi, sizeof(rivi), stdin) == NULL) {
        return -1; /* fgets() yrittää lukea tyhjään bufferiin */
    }

    /* Katso paa_valikko() */
    if (rivi[strlen(rivi) - 1] != '\n') {
        int c;
        while ((c = fgetc(stdin)) != EOF && c != '\n');
    }

    /* Lue ja tarkista valinta */
    if (sscanf(rivi, "%d", &valinta) != 1) {
        return -1; /* Virheellinen syöte (syöte ei ole numero) */
    }

    if (valinta == indeksi) {
        return 0; /* Tulosta kuitti */
    }

    if(valinta <= 0 || valinta > indeksi) {
        return -1; /* Virheellinen syöte (annettu numero ei ole listalla) */
    }

    return valinta;
}


/**
 * Laskee kokonaispantin määrän
 * Käytetään kun lasketaan kuitin ja päälokin pantteja
 */
float kokonaispantti(Tuote* tuotelista, Palautus* palautuslista) {
    int indeksi = 1;
    float yhteensa = 0.0f;
    Tuote* nykyinen_tuote = tuotelista;
    Palautus* nykyinen_palautus = palautuslista;

    while (nykyinen_tuote != NULL) {
        while (nykyinen_palautus != NULL) {
            if (nykyinen_palautus->index == indeksi) {
                /* Palautus löytyi */
                yhteensa += nykyinen_tuote->pantti;
            }

            nykyinen_palautus = nykyinen_palautus->pSeuraava;
        }

        /* Nollataan palautukset */
        nykyinen_palautus = palautuslista; /* Palataan palautuslistan alkuun */

        /* Siirry seuraavaan tuotteeseen */
        nykyinen_tuote = nykyinen_tuote->pSeuraava;
        indeksi++;
    }

    return yhteensa;
}

void kuitti(Tuote* tuotelista, Palautus* palautuslista) {
    int indeksi = 1;
    Tuote* nykyinen_tuote = tuotelista;
    int nykyinen_tuote_maara = 0; /* Ylemmän tuotteen määrä */
    Palautus* nykyinen_palautus = palautuslista;

    #if VIOPE
    printf("\n\nKuitti\n\n");
    #else
    char aika[TIMESTAMP_LENGTH];
    timestamp(aika);
    printf("\n\n\nKuitti %s\n\n", aika);
    #endif

    printf("Palautetut pullot ja tölkit yhteensä %d kappaletta.\n\n", palautus_pituus(palautuslista));

    /**
     * Panttien laskeminen tapahtuu käymällä läpi tuotelista ja ottamalla
     * sieltä muistiin yksi tuote kerrallaan.
     * Sitten käydään kokonaisuudessaan läpi palautuslista ja katsotaan kuinka monta
     * saman tyyppistä tuotetta siellä on.
     */
    while (nykyinen_tuote != NULL) {
        while (nykyinen_palautus != NULL) {
            if (nykyinen_palautus->index == indeksi) {
                /* Palautus löytyi */
                nykyinen_tuote_maara++;
            }

            nykyinen_palautus = nykyinen_palautus->pSeuraava;
        }

        if (nykyinen_tuote_maara > 0) {
            /* Tulosta palautusrivi */
            printf("%s %.2fl\t\tpantit %d x %.2f = %.2f€\n",
            nykyinen_tuote->tyyppi,
            nykyinen_tuote->koko,
            nykyinen_tuote_maara,
            nykyinen_tuote->pantti,
            nykyinen_tuote->pantti * nykyinen_tuote_maara);
        }

        /* Nollataan palautukset */
        nykyinen_palautus = palautuslista; /* Palataan palautuslistan alkuun */
        nykyinen_tuote_maara = 0;

        /* Siirry seuraavaan tuotteeseen */
        nykyinen_tuote = nykyinen_tuote->pSeuraava;
        indeksi++;
    }

    printf("\n\nPantit yhteensä %.2f€\n", kokonaispantti(tuotelista, palautuslista));
}
